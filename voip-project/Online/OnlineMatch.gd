class_name OnlineMatch
extends Node

## Represents a single match of gameplay.
##
## Add to a scene and call one of the appropriate public methods to establish an online connection.
## Free the node to automatically disconnect from the match.
##

#----------------------------------------#
#   ∙   ∙   ∙   ∙SIGNALS   ∙   ∙   ∙   ∙ #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙   ∙ ENUMS   ∙   ∙   ∙   ∙  #
#----------------------------------------#
enum OP_CODE {
	NULL = 0,
	PEER_ID,
	PEER_ID_MATCHMAKE,
	PEER_ID_MATCHMAKE_RECEIVED_RESP,
	PEER_ID_LOBBY_JOIN_NEW,				# Tell the new player their ID
	PEER_ID_LOBBY_JOIN_OTHERS,			# Tell everyone else about the new player's ID
	PEER_ID_LOBBY_JOIN_OTHERS_TO_NEW,	# Tell the new player about everyone else
	WEBRTC_OFFER,
	WEBRTC_ANSWER,
	ICE_READY,
	ICE_CANDIDATE
}

#----------------------------------------#
#   ∙   ∙   ∙   CONSTANTS   ∙   ∙   ∙   ∙#
#----------------------------------------#
## String delimiter used in sending some packets with multiple pieces of data.
const DATA_DELIM = char(7)

#----------------------------------------#
#   ∙   ∙   ∙   ∙EXPORTS   ∙   ∙   ∙   ∙ #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙PUBLIC VARIABLES   ∙   ∙   ∙#
#----------------------------------------#
## Is this a joined lobby game? False for a matchmake game.
var lobby := false

## Is the current player the "host"?
var host := false

## The Nakama match ID of the current match.
var match_id := ""

## The [WebRTCMultiplayerPeer] of the current instance. Can also be obtained from [MultiplayerAPI].
var peer: WebRTCMultiplayerPeer = null

#----------------------------------------#
#   ∙   ∙   PRIVATE VARIABLES   ∙   ∙   ∙#
#----------------------------------------#
## The socket for the current connection. Closing this ends the current connection to the match.
var _socket: NakamaSocket = null

# Internal connection data for the current instance #
var _my_peer_id := -1
var _my_sort_id := -1
var _my_presence: NakamaRTAPI.UserPresence = null

## Dictionary of peer ID to PeerData, stores the PeerData of all connected instances, excluding
## [i]this[/i] one.
var _peers := {}

var _sorted_ids: Array[int] = []
var _peer_count := 0
var _sort_count := 0

#----------------------------------------#
#   ∙   ∙   ONREADY VARIABLES   ∙   ∙   ∙#
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙VIRTUAL METHODS   ∙   ∙   ∙ #
#----------------------------------------#
## Cleanly quits out of the multiplayer and closes the [member _socket].
func _exit_tree() -> void:
	_quit_api_multiplayer()
	if _socket != null:
		_socket.close()

func _process(delta) -> void:
	for p in _peers.values():
		p.poll()

#----------------------------------------#
#   ∙   ∙   ∙ PUBLIC METHODS   ∙   ∙   ∙ #
#----------------------------------------#
## Creates a new lobby for players to join, with an optional name argument for the lobby.
func create_lobby(lobby_name: String = "") -> void:
	await _connect_socket()
	
	if !_is_socket_connected():
		Debug.log_warning("Trying to create a lobby with an invalid socket!")
		return
	
	_socket.received_matchmaker_matched.connect(self._on_matchmaker_matched)
	_socket.received_match_presence.connect(self._on_received_match_presence)
	_socket.received_match_state.connect(self._on_received_match_state)
	
	lobby = true
	host = true
	_setup_api_multiplayer()
	_my_sort_id = 0
	_sort_count = 1
	await _socket.create_match_async(lobby_name)

## Join a lobby (or any match) in progress with the match/lobby ID.
func join_lobby(lobby_id: String) -> void:
	await _connect_socket()
	
	if !_is_socket_connected():
		Debug.log_warning("Trying to join a lobby with an invalid socket!")
		return
	
	_socket.received_matchmaker_matched.connect(self._on_matchmaker_matched)
	_socket.received_match_presence.connect(self._on_received_match_presence)
	_socket.received_match_state.connect(self._on_received_match_state)
	
	lobby = true
	await _socket.join_match_async(lobby_id)

## Enter matchmaking (filters and parameters are WIP!)
func matchmake(_p_count: int) -> void:
	await _connect_socket()
	
	if !_is_socket_connected():
		Debug.log_warning("Trying to matchmake with an invalid socket!")
		return
	
	var p_count := Debug.PLAYER_COUNT
	if _p_count != null && _p_count > 0:
		p_count = _p_count
	
	_socket.received_matchmaker_matched.connect(self._on_matchmaker_matched)
	_socket.received_match_presence.connect(self._on_received_match_presence)
	_socket.received_match_state.connect(self._on_received_match_state)
	# Debug.log_debug(_session._user_id, "began matchmaking.")
	lobby = false
	await _socket.add_matchmaker_async("*",p_count)

## Send out a packet of data to currently connected players. If [param presences] is null, send to
## all players. [param presences] is optional, and can be either an array of presences or a single
## presence.
func send_match_data(opcode: int, data: String, presences = null) -> void:
	if _socket == null || match_id == "":
		Debug.log_error("Tried sending match data with null socket/match_id!")
		Debug.log_error("OPCode is", opcode, "and data is", data)
	else:
		var tmp = presences
		if typeof(presences) != TYPE_ARRAY:
			tmp = [presences]
		_socket.send_match_state_async(match_id, opcode, data, tmp)

## Determines if a new host is needed for lobby games. Currently not fully implemented for 
## matchmake games.
func check_new_host() -> bool:
	var host := true
	for p_id in _get_peer_ids():
		if _my_sort_id > _get_peer_data(p_id).sort_id:
			host = false
	return host

## Response from a ping.
@rpc("any_peer")
func pong() -> void:
	Debug.log(Debug.INFO, "Pong! From", multiplayer.get_remote_sender_id())

#----------------------------------------#
#   ∙   ∙   ∙PRIVATE METHODS   ∙   ∙   ∙ #
#----------------------------------------#
func _connect_socket() -> void:
	if !Online.is_session_valid():
		Debug.log(Debug.WARNING, "Tried to connect Nakama socket with invalid session.")
		return
		
	if _socket != null && _socket.is_connected_to_host():
		return
		
	_socket = Nakama.create_socket_from(Online.get_client())
	
	_socket.connected.connect(self._on_socket_connected)
	_socket.closed.connect(self._on_socket_closed)
	_socket.connection_error.connect(self._on_socket_connection_error)
	_socket.received_error.connect(self._on_socket_error)
	
	await _socket.connect_async(Online.get_session(), true, 60)

func _is_socket_connected() -> bool:
	return _socket != null && _socket.is_connected_to_host()

## Generates a new peer ID randomly, ensuring that it doesn't match anything in [param exclude].
func _generate_peer_id(exclude: Array[int] = []) -> int:
	var id := -1
	
	if exclude.size() > 0:
		id = exclude.front()
		while id in exclude:
			id = randi_range(2,2147483647)
	else:
		id = randi_range(2,2147483647)
	
	return id

## Initializes everything needed for Godot's high-level networking, including the WebRTC peer.
func _setup_api_multiplayer(peer_id: int = 0, exclude: Array[int] = []) -> void:
	if peer_id == 0:
		_my_peer_id = _generate_peer_id(exclude)
	else:
		_my_peer_id = peer_id
	
	peer = WebRTCMultiplayerPeer.new()
	peer.create_mesh(_my_peer_id)
	set_multiplayer_authority(_my_peer_id)
	
	var api := get_tree().get_multiplayer()
	api.multiplayer_peer = peer
	
	api.peer_connected.connect(_on_peer_connected)
	api.peer_disconnected.connect(_on_peer_disconnected)
	
	get_tree().set_multiplayer(api)
	
func _quit_api_multiplayer() -> void:
	peer = null
	for p in _peers:
		_peers[p].close()
		_peers.erase(p)

#-------------------#
# PEER DATA METHODS #
#-------------------#

## Adds a new [param peer_id], [param presence], and optionally [param sort_id] to the local
## PeerData dictionary. This does not immediately establish connections! If [param from_matchmaking],
## it will also send a message communicating if it's received all players' datas.
func _add_peer_to_dict(peer_id: int, presence: NakamaRTAPI.UserPresence, sort_id: int = -1, from_matchmake: bool = false) -> void:
	if peer_id == null || presence == null:
		return
	
	if from_matchmake && _peers.has(peer_id):
		Debug.log_error("This is literally a 1 in 4,294,967,296 error, congratulations! You should play the lottery.")
		# TODO: Return to main menu, end match for all
	
	if !_peers.has(peer_id):
		_peers[peer_id] = PeerData.new(presence, peer, peer_id, sort_id, self)
	if from_matchmake && _peers.size() == _peer_count-1:
		send_match_data(OP_CODE.PEER_ID_MATCHMAKE_RECEIVED_RESP, "")

func _remove_peer_from_dict(presence: NakamaRTAPI.UserPresence) -> void:
	_peers.erase(_get_peer_id(presence))

func _get_peer_data(peer_id: int) -> PeerData:
	return _peers.get(peer_id)

func _get_peer_from_presence(presence: NakamaRTAPI.UserPresence) -> PeerData:
	return _get_peer_data(_get_peer_id(presence))

func _get_presence(peer_id: int) -> NakamaRTAPI.UserPresence:
	return _peers.get(peer_id).presence
	
func _get_peer_id(presence: NakamaRTAPI.UserPresence) -> int:
	if presence == null:
		return -1
	var temp = null
	for p in _peers.values():
		if p.presence.user_id == presence.user_id:
			temp = _peers.find_key(p)
	return temp if (temp != null) else -1

## Gets all peer IDs from the local PeerData dictionary. This may not be accurate depending on when
## it's called! It may be better to pull directly from [MultiplayerAPI].
func _get_peer_ids() -> Array[int]:
	var arr: Array[int]
	arr.assign(_peers.keys().duplicate())
	return arr

func _get_highest_sort_id() -> int:
	var highest := 0
	for p_id in _get_peer_ids():
		if _get_peer_data(p_id).sort_id > highest:
			highest = _get_peer_data(p_id).sort_id
	return highest

## Used to generate sort IDs based off of every player's peer ID. Sort IDs are essentially IDs from
## [0, <player_count>) that are used to establish a sort of "hierarchy". Otherwise, all players have
## equal authority in matchmake games, which is not good.
## [br]Assumes there's PeerData already generated for all players.
func _populate_sort_ids() -> void:
	_sorted_ids.assign(_peers.keys().duplicate())
	_sorted_ids.append(_my_peer_id)
	_sorted_ids.sort()
	for i in range(0,_sorted_ids.size()):
		if _sorted_ids[i] == _my_peer_id:
			_my_sort_id = i
		else:
			_peers[_sorted_ids[i]].sort_id = i
	
	if _my_sort_id == 0:
		host = true

func _check_webrtc_ready() -> bool:
	for p in _peers.values():
		if !p.webrtc_ready:
			return false
	return true

func _send_webrtc_offer(peer_id: int) -> void:
	_get_peer_data(peer_id).init_webrtc_offer(_my_sort_id)

func _send_matchmaked_webrtc_offers() -> void:
	_populate_sort_ids()
	# Loop over IDs greater than our own
	for i in range(_my_sort_id+1, _sorted_ids.size()):
		_peers[_sorted_ids[i]].init_webrtc_offer(_my_sort_id)

#----------------------------------------#
#   ∙   ∙   ∙SIGNAL LISTENERS   ∙   ∙   ∙#
#----------------------------------------#
func _on_socket_connected() -> void:
	Debug.log(Debug.INFO, "Socket connected.")

func _on_socket_closed() -> void:
	Debug.log(Debug.INFO, "Socket closed.")

func _on_socket_connection_error(err) -> void:
	Debug.log(Debug.WARNING, "Socket connection error:", err)

func _on_socket_error(err) -> void:
	Debug.log(Debug.ERROR, "Socket error:", err)

## Processes joins and leaves in a match.
func _on_received_match_presence(p_match_presence_event) -> void:
	var event: NakamaRTAPI.MatchPresenceEvent = p_match_presence_event
	Debug.log_debug(Online._session.username, "got a match presence event:", event)
	match_id = event.match_id
	
	for presence in event.leaves:
		presence = presence as NakamaRTAPI.UserPresence
		peer.disconnect_peer(_get_peer_id(presence))
		_remove_peer_from_dict(presence)
		
		if check_new_host() && !host:
			Debug.log_info(Online._session.username, "has become the new host.")
			host = true
			_sort_count = _get_highest_sort_id() + 1
	
	# Everything beyond here is irrelevant to matchmakers
	if !lobby:
		return
	
	for presence in event.joins:
		presence = presence as NakamaRTAPI.UserPresence
		if presence.user_id == Online.get_user_id():
			_my_presence = presence	# This is nestled in here to save our presence in case the host DCs
			continue				# and we become the new host (and need to communicate our presence)
		
		if !host:	# Only the host needs to send out IDs and match info
			return
		
		# Generate the new ID and send it to them first
		var their_peer_id := _generate_peer_id(_get_peer_ids())
		var their_sort_id := _sort_count
		_sort_count += 1
		send_match_data(OP_CODE.PEER_ID_LOBBY_JOIN_NEW, var_to_str([their_peer_id, their_sort_id]), presence)
		
		var others_data := []
		for p_id in _get_peer_ids():
			# Send everyone else the new ID
			send_match_data(OP_CODE.PEER_ID_LOBBY_JOIN_OTHERS, 
				var_to_str([their_peer_id, their_sort_id, presence.serialize()]), _get_presence(p_id))
			
			# Append their data
			others_data.append([p_id, _get_peer_data(p_id).sort_id, _get_presence(p_id).serialize()])
		
		# Append our own data
		others_data.append([_my_peer_id, _my_sort_id, _my_presence.serialize()])
		
		# Add the new player to our peers
		_add_peer_to_dict(their_peer_id, presence, their_sort_id)
		
		await get_tree().create_timer(0.3)
		
		# Send the new player everyone else's info
		send_match_data(OP_CODE.PEER_ID_LOBBY_JOIN_OTHERS_TO_NEW, var_to_str(others_data), presence)
		
## Called on all instances when players find a match to join from matchmaking.
func _on_matchmaker_matched(p_matched) -> void:
	p_matched = p_matched as NakamaRTAPI.MatchmakerMatched
	var joined_match = await _socket.join_matched_async(p_matched)
	
	_peer_count = p_matched.users.size()
	match_id = joined_match.match_id
	
	_setup_api_multiplayer()
	
	send_match_data(OP_CODE.PEER_ID_MATCHMAKE, str(_my_peer_id))

## Called for every message received.
func _on_received_match_state(p_match_state) -> void:
	var presence: NakamaRTAPI.UserPresence = p_match_state.presence
	var rec_peer := _get_peer_from_presence(presence)
	var data: String = p_match_state.data
	var op_code: int = p_match_state.op_code
	
	Debug.log(Debug.DEBUG, Online._session.username, "RECEIVED FROM", presence.username, "|>", op_code, "|", data)
	
	match (op_code):
		OP_CODE.NULL:
			Debug.log(Debug.WARNING, "Null op_code from", presence.username)
		OP_CODE.PEER_ID:
			var arr: Array = str_to_var(data)
			_add_peer_to_dict(arr[0], presence, arr[1])
		OP_CODE.PEER_ID_MATCHMAKE:
			_add_peer_to_dict(data.to_int(), presence, -1, true)
		OP_CODE.PEER_ID_MATCHMAKE_RECEIVED_RESP:
			rec_peer.webrtc_ready = true
			if _check_webrtc_ready():
				_send_matchmaked_webrtc_offers()
		OP_CODE.PEER_ID_LOBBY_JOIN_NEW:
			var arr: Array = str_to_var(data)
			_setup_api_multiplayer(arr[0])
			_my_sort_id = arr[1]
		OP_CODE.PEER_ID_LOBBY_JOIN_OTHERS:
			var arr: Array = str_to_var(data)
			var p_id: int = arr[0]
			var p_sort_id: int = arr[1]
			var p_presence := NakamaRTAPI.UserPresence.create(NakamaRTAPI, arr[2])
			
			_add_peer_to_dict(p_id, p_presence, p_sort_id)
		OP_CODE.PEER_ID_LOBBY_JOIN_OTHERS_TO_NEW:
			var big_arr: Array = str_to_var(data)
			for arr in big_arr:
				var p_id: int = arr[0]
				var p_sort_id: int = arr[1]
				var p_presence := NakamaRTAPI.UserPresence.create(NakamaRTAPI, arr[2])
				_add_peer_to_dict(p_id, p_presence, p_sort_id)
				
				_send_webrtc_offer(p_id)
			
		OP_CODE.WEBRTC_OFFER:
			var type := data.get_slice(DATA_DELIM, 0)
			var sdp := data.get_slice(DATA_DELIM, 1)
			rec_peer.receive_webrtc_offer(_my_sort_id, type, sdp)
		OP_CODE.WEBRTC_ANSWER:
			var type := data.get_slice(DATA_DELIM, 0)
			var sdp := data.get_slice(DATA_DELIM, 1)
			rec_peer.receive_webrtc_answer(type, sdp)
		OP_CODE.ICE_READY:
			rec_peer.ready_ice()
		OP_CODE.ICE_CANDIDATE:
			var media := data.get_slice(DATA_DELIM, 0)
			var index := data.get_slice(DATA_DELIM, 1).to_int()
			var ice_name := data.get_slice(DATA_DELIM, 2)
			rec_peer.add_ice_candidate(media, index, ice_name)
		_:
			Debug.log(Debug.WARNING, "Unexpected op_code from", presence.username)

## Called when a peer fully connects, via Godot's high-level networking.
func _on_peer_connected(id: int) -> void:
	Debug.log_debug(id, "is Godot-nnected to", _my_peer_id)

## Called when a peer disconnects. For WebRTC mesh networking, is not guaranteed to be called on all
## peers! Rely on [method _on_received_match_presence] instead.
func _on_peer_disconnected(id: int) -> void:
	Debug.log_debug(id, "Godot disconnected from", _my_peer_id)
	_peers.erase(id)

#----------------------------------------#
#   ∙   ∙   ∙   SUBCLASSES   ∙   ∙   ∙   #
#----------------------------------------#
## Big class that stores connection data for WebRTC connections. Each initialized instance of this
## class represents a single one-way connection to another player. The other player will have a
## mirror instance of this class on their end that communicates back.
## [br]Is stored in [member _peers].
class PeerData:
	class IceCandidate:
		var media: String
		var index: int
		var ice_name: String
		
		func _init(_media: String, _index: int, _ice_name: String):
			media = _media
			index = _index
			ice_name = _ice_name
			
		func to_str() -> String:
			return str(media + DATA_DELIM + str(index) + DATA_DELIM + ice_name)
	
	var init_dict = {
		"iceServers": [
			{
				"urls": [ "stun.l.google.com:19302", "stun1.l.google.com:19302", "stun2.l.google.com:19302", "stun3.l.google.com:19302","stun4.l.google.com:19302" ], # One or more STUN servers.
			},
#			{
#				"urls": [ "" ], # One or more TURN servers.
#				"username": "", # Optional username for the TURN server.
#				"credential": "", # Optional password for the TURN server.
#			}
		]
	}
	
	var data_dict = {
		"negotiated": true, # When set to true (default off), means the channel is negotiated out of band. "id" must be set too. "data_channel_received" will not be called.
		"id": 0, # When "negotiated" is true this value must also be set to the same value on both peer.
		# Only one of maxRetransmits and maxPacketLifeTime can be specified, not both. They make the channel unreliable (but also better at real time).
		#"maxRetransmits": 3, # Specify the maximum number of attempt the peer will make to retransmits packets if they are not acknowledged.
		"maxPacketLifeTime": 100, # Specify the maximum amount of time before giving up retransmitions of unacknowledged packets (in milliseconds).
		"ordered": true, # When in unreliable mode (i.e. either "maxRetransmits" or "maxPacketLifetime" is set), "ordered" (true by default) specify if packet ordering is to be enforced.
		#"protocol": "", # A custom sub-protocol string for this channel.
	}
	
	var presence: NakamaRTAPI.UserPresence = null
	var peer_id := -1
	var sort_id := -1
	var webrtc_ready := false
	var peer_connection: WebRTCPeerConnection = null
	var data_channel: WebRTCDataChannel = null
	var data_channel_id := -1
	var ice_candidates: Array[IceCandidate] = []
	var ice_ready := false
	var ice_count := 0
	
	var online: Node
	
	func close():
		if data_channel != null:
			data_channel.close()
		if peer_connection != null:
			peer_connection.close()
	
	func _init(_presence: NakamaRTAPI.UserPresence, _peer: WebRTCMultiplayerPeer, _id: int, _sort_id: int, OnlineMatch: Node) -> void:
		presence = _presence
		peer_id = _id
		online = OnlineMatch
		sort_id = _sort_id
		
		peer_connection = WebRTCPeerConnection.new()
		peer_connection.initialize(init_dict)
		
		add_peer(_peer)
		
	func add_peer(peer: WebRTCMultiplayerPeer) -> void:
		var err = peer.add_peer(peer_connection, peer_id)
		if (err != OK): Debug.log_error("Error adding peer", peer_id, "to the mesh network from", online._my_peer_id, "with error code:", err)
		else: Debug.log_debug("Peer", peer_id, "was added to the mesh network from", online._my_peer_id)
		
	func init_webrtc_offer(_my_sort_id: int) -> void:
		Debug.log(Debug.DEBUG, "WebRTC offer intializing to", peer_id)
		
		data_dict["id"] = generate_data_channel_id(_my_sort_id, sort_id)
		Debug.log(Debug.DEBUG, "Data Channel ID:", data_dict["id"])
		data_channel = peer_connection.create_data_channel(str(peer_id), data_dict)
		
		peer_connection.ice_candidate_created.connect(self._on_ice_candidate_created)
		peer_connection.session_description_created.connect(self._on_session_description_offer_created)
		peer_connection.create_offer()
	
	func receive_webrtc_offer(_my_sort_id: int, type: String, sdp: String) -> void:
		Debug.log(Debug.DEBUG, "WebRTC offer received from", peer_id)
		
		data_dict["id"] = generate_data_channel_id(sort_id, _my_sort_id)
		Debug.log(Debug.DEBUG, "Data Channel ID:", data_dict["id"])
		data_channel = peer_connection.create_data_channel(str(peer_id), data_dict)
		
		peer_connection.session_description_created.connect(self._on_session_description_answer_created)
		peer_connection.ice_candidate_created.connect(self._on_ice_candidate_created)
		peer_connection.set_remote_description(type, sdp)
		
	func receive_webrtc_answer(type: String, sdp: String) -> void:
		peer_connection.set_remote_description(type, sdp)
	
	func _on_session_description_offer_created(type: String, sdp: String) -> void:
		Debug.log(Debug.DEBUG, "OFFER CREATED WITH TYPE", type)
		peer_connection.set_local_description(type, sdp)
		online.send_match_data(online.OP_CODE.WEBRTC_OFFER, type+online.DATA_DELIM+sdp, presence)
		
	func _on_session_description_answer_created(type: String, sdp: String) -> void:
		Debug.log(Debug.DEBUG, "ANSWER CREATED WITH TYPE", type)
		peer_connection.set_local_description(type, sdp)
		online.send_match_data(online.OP_CODE.WEBRTC_ANSWER, type+online.DATA_DELIM+sdp, presence)
	
	func _on_ice_candidate_created(media: String, index: int, ice_name: String) -> void:
		#Debug.log(Debug.INFO, "ICE CANDIDATE CREATED")
		Debug.log(Debug.DEBUG, "ICE CANDIDATE CREATED", online._my_peer_id, media, "|", index, "|", ice_name)
		Debug.log(Debug.DEBUG, "STATE:",peer_connection.get_signaling_state())
		ice_candidates.append(IceCandidate.new(media, index, ice_name))
		if (ice_candidates.size() >= 3):
			Debug.log(Debug.DEBUG, "Ready to send and receive ice candidates", online._my_peer_id)
			online.send_match_data(online.OP_CODE.ICE_READY, "", presence)
			if (ice_ready):
				send_ice_candidates()
	
	func ready_ice() -> void:
		ice_ready = true
		if (ice_candidates.size() >= 3):
			send_ice_candidates()
	
	func send_ice_candidates() -> void:
		for ice in ice_candidates:
			online.send_match_data(online.OP_CODE.ICE_CANDIDATE, ice.to_str(), presence)
			
	func add_ice_candidate(media: String, index: int, ice_name: String) -> void:
		peer_connection.add_ice_candidate(media, index, ice_name)
#		if (ice_count >= 3):
#			var err = Online.peer.add_peer(peer_connection, peer_id)
#			if (err != OK): Debug.log(Debug.INFO, "Error adding peer", peer_id, "to the mesh network from", Online._my_peer_id, "with error code:", err)
#			else: Debug.log(Debug.INFO, "Peer", peer_id, "was added to the mesh network from", Online._my_peer_id)
	
	static func generate_data_channel_id(peer_a_sort: int, peer_b_sort: int) -> int:
		return (peer_a_sort * 100) + (peer_b_sort - peer_a_sort)
	
	func poll() -> void:
		if peer_connection != null:
			peer_connection.poll()

