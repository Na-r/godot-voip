extends Node

## Autoload to handle all online actions.
##
## Establishes a connection to the Nakama server, and handles creating/joining matches, matchmaking,
## reading/writing cloud data, and other similar functionality. Does not control actual gameplay
## connections. For that, see [OnlineMatch].
##

#----------------------------------------#
#   ∙   ∙   ∙   ∙SIGNALS   ∙   ∙   ∙   ∙ #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙   ∙ ENUMS   ∙   ∙   ∙   ∙  #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙   CONSTANTS   ∙   ∙   ∙   ∙#
#----------------------------------------#
## Scheme of the Nakama server, needs to be updated to HTTPS before release.
const SCHEME := "http"
## IP of the Nakama server.
const HOST := "132.226.42.125"
## Port to connect to the Nakama server on.
const PORT := 7350
## The key to connect to the Nakama server with. Will be changed and encrypted before release.
const SERVER_KEY := "defaultkey"

## The file path for the authentication token saved after a successful Nakama login.
const TOKEN_FILE_PATH := "user://authtoken.txt"

#----------------------------------------#
#   ∙   ∙   ∙   ∙EXPORTS   ∙   ∙   ∙   ∙ #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙PUBLIC VARIABLES   ∙   ∙   ∙#
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   PRIVATE VARIABLES   ∙   ∙   ∙#
#----------------------------------------#
var _client: NakamaClient = null
var _session: NakamaSession = null
var _online_match: OnlineMatch = null

#----------------------------------------#
#   ∙   ∙   ONREADY VARIABLES   ∙   ∙   ∙#
#----------------------------------------#
@onready var online_match_node := load("uid://drv5fme4bvquy")

#----------------------------------------#
#   ∙   ∙   ∙VIRTUAL METHODS   ∙   ∙   ∙ #
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙ PUBLIC METHODS   ∙   ∙   ∙ #
#----------------------------------------#
func get_client() -> NakamaClient:
	return _client
	
func get_session() -> NakamaSession:
	return _session

## Returns the current [OnlineMatch], if it exists.
func get_match() -> Node:
	if _online_match == null:
		Debug.log_error("Trying to get a null OnlineMatch!")
	return _online_match

## Get the current player's Nakama User ID.
func get_user_id() -> String:
	return get_session().user_id

## Check if the session is valid. In other words, checks if we're connected to Nakama.
func is_session_valid() -> bool:
	return !(_session == null || _session.is_expired())

## Establish a connection to Nakama. Needs to be done before attempting any other Online methods!
func connect_to_nakama(email: String, password: String, username: String, restore: bool = true) -> void:
	_client = Nakama.create_client(SERVER_KEY, HOST, PORT, SCHEME)
	if restore: _session = NakamaClient.restore_session(_load_session_token())
	
	if _session == null || _session.is_expired():
		_session = await _client.authenticate_email_async(email, password, username)
		Debug.log(Debug.INFO, "Created new Nakama session.")
		_save_session_token(_session.token)
	else:
		Debug.log(Debug.INFO, "Restored Nakama session successfully.")
		
	if _session.is_exception():
		Debug.log(Debug.ERROR, "Failed to establish Nakama session.")

## Creates a new lobby for players to join, with an optional name argument for the lobby.
func create_lobby(lobby_name: String = "") -> void:
	_create_match()
	_online_match.create_lobby(lobby_name)

## Join a lobby (or any match) in progress with the match/lobby ID.
func join_lobby(lobby_id: String) -> void:
	_create_match()
	_online_match.join_lobby(lobby_id)

## Enter matchmaking (filters and parameters are WIP!)
func matchmake(player_count: int) -> void:
	_create_match()
	_online_match.matchmake(player_count)

## Leave the current match (if it exists.)
func leave_match() -> void:
	if _online_match == null:
		return
	_online_match.queue_free()
	_online_match = null

#----------------------------------------#
#   ∙   ∙   ∙PRIVATE METHODS   ∙   ∙   ∙ #
#----------------------------------------#
func _save_session_token(token: String):
	var file := FileAccess.open(TOKEN_FILE_PATH, FileAccess.WRITE)
	if file == null:
		Debug.log(Debug.ERROR, "Failed to save session token, got file error:", FileAccess.get_open_error())
		return
	
	file.store_line(token)
	Debug.log(Debug.INFO, "Saved Nakama session token to file.")
	file = null

func _load_session_token():
	var temp := ""
	if FileAccess.file_exists(TOKEN_FILE_PATH):
		var file := FileAccess.open(TOKEN_FILE_PATH, FileAccess.READ)
		if file == null:
			Debug.log(Debug.ERROR, "Failed to load session token, got file error", FileAccess.get_open_error())
			return ""
		
		temp = file.get_as_text()
		file = null
	
	Debug.log(Debug.INFO, "Loaded Nakama session token from file.")
	return temp

func _create_match() -> void:
	if _online_match != null:
		leave_match()
	
	_online_match = online_match_node.instantiate()
	add_child(_online_match)

#----------------------------------------#
#   ∙   ∙   ∙SIGNAL LISTENERS   ∙   ∙   ∙#
#----------------------------------------#


#----------------------------------------#
#   ∙   ∙   ∙   SUBCLASSES   ∙   ∙   ∙   #
#----------------------------------------#





