extends Node

const PLAYER_COUNT = 2

enum {
	DEBUG,
	INFO,
	WARNING,
	ERROR
}

func log(lvl: int, arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	_internal_log(lvl, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)

func log_info(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	_internal_log(INFO, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	
func log_debug(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	_internal_log(DEBUG, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)

func log_warning(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	_internal_log(WARNING, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
	
func log_error(arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	_internal_log(ERROR, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)

func _internal_log(lvl: int, arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	var msg := ""
	for arg in [arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9]:
		if arg != null:
			if typeof(arg) == TYPE_OBJECT && arg.has_method("to_string"):
				msg += arg.to_string() + " "
			else:
				msg += str(arg) + " "
	
	var temp := ""
	match (lvl):
		DEBUG:
			temp = "[color=yellow]DEBUG: [/color]"
		INFO:
			temp = "LOG: "
		WARNING:
			temp = "[color=orange]WARNING: [/color]"
			push_warning(msg)
		ERROR:
			temp = "ERROR: "
			push_error(msg)
			printerr(temp + msg)
	
	var full_log := "[b]" + temp + "[/b]" + msg
	add_log(full_log)
	print_log()
	if (lvl != ERROR): print_rich(full_log)

##### Following functions are only used for an ingame log
##### by setting TEXT_BOX to a RichTextLabel

const LOG_COUNT = 5
var TEXT_BOX: RichTextLabel
var logs: Array[String] = []

func add_log(str: String):
	if (logs.size()>LOG_COUNT):
		logs.pop_back()
	logs.push_front(str)

func print_log():
	if TEXT_BOX:
		TEXT_BOX.text = ""
		for str in logs:
			TEXT_BOX.text += str + "\n"

#func _input(event):
#	if event.is_action_pressed("Debug"):
#		Signals._boss_spawn.emit(Boss.Type.Nim)
#	elif event.is_action_pressed("Debug2"):
#		PlayerData.add_bitz(Big.new(10, 5))
#	elif event.is_action_pressed("DebugPopcat"):
#		Signals.spawn_bonus.emit(Bonus.BonusType.Popcat, Bonus.BonusTier.Normal)
	
#	if event.is_action_pressed("OpenDebugMenu"):
#		if OS.is_debug_build():
#			Signals.toggle_debug_menu.emit()
