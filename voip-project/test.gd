extends Node2D

@onready var mic = $VoipMicrophone
@onready var speaker = $VoipSpeaker
@onready var label = $Label

@onready var mix_rate: int = ProjectSettings.get_setting("audio/driver/mix_rate", 44100)
var packet_loss := 0.0

# Change the packet loss percentage
func _on_h_slider_value_changed(value):
	label.text = "Packet Loss: " + str(value) + "%"
	packet_loss = value/100.0

# Play Billy over the VOIP connection
func _on_button_pressed():
	$AudioStreamPlayer.play()

# RPC to be called when sending out voice data
@rpc("any_peer", "call_remote")
func send_voice(data: PackedByteArray):
	if (randf() < packet_loss): data = []
	speaker.receive_voice(data)

# Not actually a host, tests the voip over a WebRTC p2p connection
# THIS WILL MOST LIKELY NOT WORK FOR YOU, this is using a WebRTC server that was
# configured purely for this test
func _on_host_pressed():
	Online.connect_to_nakama("1@debug.com", "password", "debug1", false)
	await get_tree().create_timer(1).timeout
	Online.matchmake(2)

# Just another account for the WebRTC signaling server
func _on_join_pressed():
	Online.connect_to_nakama("2@debug.com", "password", "debug2", false)
	await get_tree().create_timer(1).timeout
	Online.matchmake(2)

# Ping rotates the icon to ensure there's an actual connection
@rpc("any_peer", "call_remote")
func ping():
	$Sprite2D.rotate(1)
	print(multiplayer.get_remote_sender_id(), ">", multiplayer.get_unique_id())

# Start up the local mic
func _on_enable_mic_pressed():
	mic.opus_mic_processed.connect(func(data: PackedByteArray): 
		self.send_voice.rpc(data)
	)
	mic.init()

# Start up the local speaker, fixed sample rate
# Having a fixed sample rate is flawed
func _on_enable_speaker_pressed():
	speaker.init(mix_rate)
	print("speakinit")

# Start up a local test of the VOIP, so you'll hear your own mic over your own instance
func _on_local_pressed() -> void:
	mic.opus_mic_processed.connect(func(data: PackedByteArray): 
		send_voice(data)
	)
	mic.init()
	speaker.init(mix_rate)

# Sanity check if running multiple instances - plays test audio on the local instance.
# This only exists so it's easier to tell which instance is playing what audio, because otherwise
# they all look the same in the sound/volume manager.
func _on_button_2_pressed() -> void:
	$AudioStreamPlayerLocal.play()

func _on_ping_pressed() -> void:
	ping.rpc()
