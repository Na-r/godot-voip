extends Control

func _on_ping_pressed():
	var m := Online.get_match()
	if m != null:
		m.pong.rpc()
	var api := get_tree().get_multiplayer()
	var msg := "Unique ID: " + str(api.get_unique_id()) + "\n" + "Peers: " + str(api.get_peers())
	$ConnectionInfo.text = msg

func _process(delta):
	pass#$PeerData.text = str(get_tree().get_multiplayer().get_peers())


func _on_connect_1_pressed():
	Online.connect_to_nakama("1@debug.com", "password", "debug1", false)

func _on_connect_2_pressed():
	Online.connect_to_nakama("2@debug.com", "password", "debug2", false)

func _on_matchmake_pressed():
	Online.matchmake(Debug.PLAYER_COUNT)
