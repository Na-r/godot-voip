#include "voip.h"

#include <deque>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>

#include <godot_cpp/classes/audio_server.hpp>
#include <godot_cpp/classes/audio_stream_microphone.hpp>
#include <godot_cpp/classes/audio_effect_capture.hpp>
#include <godot_cpp/classes/audio_stream_generator.hpp>
#include <godot_cpp/classes/audio_stream_generator_playback.hpp>

#define SAMPLE_RATE 48000
#define FRAME_SIZE 960
#define CHANNELS 2
#define APPLICATION OPUS_APPLICATION_VOIP
#define BITRATE 64000

#define MAX_FRAME_SIZE 6*FRAME_SIZE
#define MAX_PACKET_SIZE 3*1276

using namespace godot;

/* VOIP Microphone */

void VoipMicrophone::_bind_methods() {
    ClassDB::bind_method(D_METHOD("init"), &VoipMicrophone::init);

    ADD_SIGNAL(MethodInfo("opus_mic_processed", PropertyInfo(Variant::PACKED_BYTE_ARRAY, "data")));
}

VoipMicrophone::VoipMicrophone() {
    int err = -1;
    encoder = opus_encoder_create(SAMPLE_RATE, CHANNELS, APPLICATION, &err);

    if (err < 0) {
        UtilityFunctions::printerr("VOIPMICROPHONE: FAILED TO INIT ENCODER");
        UtilityFunctions::printerr(opus_strerror(err));
        return;
    }

    err = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(BITRATE), OPUS_SET_PACKET_LOSS_PERC(10), OPUS_SET_INBAND_FEC(1));
    if (err < 0) {
        UtilityFunctions::printerr("VOIPMICROPHONE: FAILED TO CONFIGURE ENCODER");
        UtilityFunctions::printerr(opus_strerror(err));
        return;
    }
}

void VoipMicrophone::init() {
    int bus_index = AudioServer::get_singleton()->get_bus_index(BUS_TITLE);
    eff_capture = AudioServer::get_singleton()->get_bus_effect(bus_index,0);

    AudioStreamPlayer mic_player = *memnew(AudioStreamPlayer);
    mic_player.set_stream(memnew(AudioStreamMicrophone));
    mic_player.set_bus(BUS_TITLE);
    add_child(&mic_player, false);
    mic_player.play(0.0);
    ready = true;
}

VoipMicrophone::~VoipMicrophone() {
    opus_encoder_destroy(encoder);
}

void VoipMicrophone::_process(double delta) {
    if (!ready) {
        return;
    }

    AudioEffectCapture *eff = eff_capture.ptr();
    if (!eff || eff == nullptr) {
        UtilityFunctions::printerr("VOIPMICROPHONE: Null effect capture. Audio bus configuration is wrong?");
        return;
    }

    // always 512 or 1024 from mic input
    int frame_count = eff->get_frames_available();

    if (frame_count < FRAME_SIZE) {
        return;
    }

    PackedVector2Array raw_data = eff->get_buffer(FRAME_SIZE);

    float *pcm = new float[FRAME_SIZE*CHANNELS];
    int j = 0;
    for (int i = 0; i < FRAME_SIZE*CHANNELS; i+=2) {
        pcm[i] = raw_data[j].x;
        pcm[i+1] = raw_data[j].y;
        j++;
    }

    unsigned char* data = new unsigned char[MAX_PACKET_SIZE];
    int ret = opus_encode_float(encoder, pcm, FRAME_SIZE, data, MAX_PACKET_SIZE);

    delete(pcm);

    if (ret <= 0) {
        UtilityFunctions::printerr("VOIPMICROPHONE: opus_encode returned: ", opus_strerror(ret));
        return;
    }

    PackedByteArray godot_data;
    for (int i = 0; i < ret; i++) {
        godot_data.append(data[i]);
    }

    delete(data);
    emit_signal("opus_mic_processed", godot_data);
}

/* VOIP Speaker */

void VoipSpeaker::_bind_methods() {
    ClassDB::bind_method(D_METHOD("init", "mix_rate"), &VoipSpeaker::init);
    ClassDB::bind_method(D_METHOD("receive_voice", "data"), &VoipSpeaker::receive_voice);
}

VoipSpeaker::VoipSpeaker() {
    int err = -1;
    decoder = opus_decoder_create(SAMPLE_RATE, CHANNELS, &err);
    if (err < 0) {
        UtilityFunctions::printerr("VOIPSPEAKER: FAILED TO INIT DECODER");
        UtilityFunctions::printerr(opus_strerror(err));
        return;
    }
}

void VoipSpeaker::init(double mix_rate) {
    set_playback_generator(mix_rate);
    set_bus("Master");
    set_mix_target(AudioStreamPlayer::MIX_TARGET_CENTER);
    ready = true;
}

void VoipSpeaker::set_playback_generator(double mix_rate) {
    Ref<AudioStreamGenerator> gen = Ref<AudioStreamGenerator>(memnew(AudioStreamGenerator));
    gen->set_buffer_length(0.5);
    gen->set_mix_rate(mix_rate);
    set_stream(gen);

    play();
    playback_ref = (Ref<AudioStreamGeneratorPlayback>)get_stream_playback();
}

VoipSpeaker::~VoipSpeaker() {
    opus_decoder_destroy(decoder);
}

void VoipSpeaker::receive_voice(PackedByteArray data) {
    int output_samples;
    int bytes = data.size();
    float *decoded_opus = new float[MAX_FRAME_SIZE*CHANNELS];
    const bool lost = (bytes == 0);
    if (lost) {
        opus_decoder_ctl(decoder, OPUS_GET_LAST_PACKET_DURATION(&bytes));
        output_samples = opus_decode_float(decoder, NULL, 0, decoded_opus, FRAME_SIZE, 1);
        UtilityFunctions::print("L O S T | ", bytes);
    } else {
        unsigned char* char_data = new unsigned char[bytes];
        for (int i = 0; i < bytes; i++) {
            char_data[i] = data[i];
        }
        output_samples = opus_decode_float(decoder, char_data, bytes, decoded_opus, MAX_FRAME_SIZE, 0);
        delete(char_data);
    }

    if (output_samples > 0) {
        fill_buffer(decoded_opus, output_samples*CHANNELS);
    } else {
        UtilityFunctions::printerr("VOIPSPEAKER: opus_decode returned: ", opus_strerror(output_samples));
    }

    delete(decoded_opus);
}

void VoipSpeaker::fill_buffer(float* data, int size) {
    for (int i = 0; i < size; i++) {
        data_queue.push_front(data[i]);
    }
}

void VoipSpeaker::_process(double delta) {
    if (!ready) {
        return;
    }

    AudioStreamGeneratorPlayback *playback = playback_ref.ptr();

    if (playback == nullptr) {
        UtilityFunctions::printerr("VOIPSPEAKER: Null Playback. Not initialized?");
        return;
    }

    int min = playback->get_frames_available();
    if (min < 1) {
        UtilityFunctions::printerr("VOIPSPEAKER: NO FRAMES IN PLAYBACK");
        return;
    }

    if (data_queue.size()/2 < min) {
        min = data_queue.size()/2;
    }

    for (int i = 0; i < min; i++) {
        float val1 = data_queue.back();
        data_queue.pop_back();
        float val2 = data_queue.back();
        data_queue.pop_back();

        // Technically needs to be divided by 2.0 to prevent clipping, but
        // it's fine for now
        float val3 = (val1+val2);
        playback->push_frame(Vector2(val3,val3));
    }
}