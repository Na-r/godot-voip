#ifndef VOIP_H
#define VOIP_H

#include <deque>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/audio_stream_player.hpp>
#include <godot_cpp/classes/audio_effect_capture.hpp>
#include <godot_cpp/classes/audio_stream_generator_playback.hpp>
#include <opus/opus.h>

namespace godot {

class VoipMicrophone : public Node {
    GDCLASS(VoipMicrophone, Node)

private:
    OpusEncoder *encoder;
    Ref<AudioEffectCapture> eff_capture;
    bool ready = false;
	const godot::String BUS_TITLE = "MicRecord";

protected:
    static void _bind_methods();

public:
    VoipMicrophone();
    ~VoipMicrophone();

    void init();
    void _process(double delta);
};

class VoipSpeaker : public AudioStreamPlayer {
    GDCLASS(VoipSpeaker, AudioStreamPlayer)

private:
    OpusDecoder *decoder;
	Ref<AudioStreamGeneratorPlayback> playback_ref;
	std::deque<float> data_queue;
	int frames = 0;
    bool ready = false;

    void set_playback_generator(double mix_rate);

protected:
    static void _bind_methods();

public:
    VoipSpeaker();
    ~VoipSpeaker();

    void init(double mix_rate);
    void receive_voice(PackedByteArray data);
    void fill_buffer(float* data, int size);
    void _process(double delta);
};

}

#endif